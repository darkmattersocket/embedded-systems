#include <LiquidCrystal_I2C.h>


volatile int cnt = 0;

unsigned long lastInterruptTime= 0; // Zählvariable
int debounceTime = 80;              // Prellzeit in ms

LiquidCrystal_I2C lcd(0x27, 16, 2);


void setup(){
  lcd.init();
  lcd.backlight();
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(2), inc, RISING);
  attachInterrupt(digitalPinToInterrupt(3), dec, RISING);
}
 
void loop() {
 
  lcd.clear();  
  lcd.setCursor(0, 0); // Set the cursor on the third column and first row.
  lcd.print("Count: ");
  lcd.print(cnt);
  Serial.print("Count: ");
  Serial.println(cnt);
  delay(500);
}

void inc() {
  unsigned long interruptTime = millis();
  if (interruptTime - lastInterruptTime > debounceTime) {
    Serial.println("INC");
    cnt++;
  }
  lastInterruptTime = interruptTime;
}

void dec() {
  unsigned long interruptTime = millis();
  if (interruptTime - lastInterruptTime > debounceTime) {
    Serial.println("DEC");
    cnt--;
  }
  lastInterruptTime = interruptTime;
}
