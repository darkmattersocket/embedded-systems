/*         PORT-C
  -------------------------
  | S1  | S2  | S3  | S4  | - 3 --> D34
  | S5  | S6  | S7  | S8  | - 2 --> D35
  | S9  | S10 | S11 | S12 | - 1 --> D36
  | S13 | S14 | S15 | S16 | - 0 --> D37
  -------------------------
    |      |     |     |
    4      5     6     7
   D33    D32   D31   D30

int ROWS[4] = {34,35,36,37};
int COLS[4] = {33,32,31,30};
*/ 
//int ROW = 34;                 // 1. Zeile
int ROWS[4] = {34,35,36,37};  // Reihe der 1. Zeile
int COLS[4] = {33,32,31,30};  // Spalten der 1. Zeile
int colRead[4];

uint8_t checkarray[4][4] = {
    {1, 0, 0, 0},
    {0, 1, 0, 0},
    {0, 0, 1, 0},
    {0, 0, 0, 1}
};

void setup() {

  for(int i=0;i<4;i++){
    pinMode(ROWS[i], OUTPUT);
  }

  for(int i=0;i<4;i++){
    pinMode(COLS[i], INPUT);
  }
 
  Serial.begin(9600); //For printing out the output
}
 
void loop() {

    // for (size_t i = 1; i < sizeof(checkarray); i++)
    // {
        
        for (int j = 0; j < sizeof(checkarray[0])-1; j++)
        {
            digitalWrite(ROWS[j],checkarray[0][j]);
        }
        
        
        if(digitalRead(COLS[0]) == HIGH && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
            Serial.println("1");
        }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == HIGH && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
            Serial.println("2");  
        }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == HIGH && digitalRead(COLS[3]) == LOW){
            Serial.println("3");
        }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == HIGH){
            Serial.println("4");
        }else{;}

    // }

    

    for (int i = 0; i < sizeof(checkarray[1])-1; i++)
    {
        digitalWrite(ROWS[i],checkarray[1][i]);
    }

    if(digitalRead(COLS[0]) == HIGH && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
        Serial.println("5");
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == HIGH && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
        Serial.println("6");  
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == HIGH && digitalRead(COLS[3]) == LOW){
        Serial.println("7");
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == HIGH){
        Serial.println("8");
    }else{;}

    for (int i = 0; i < sizeof(checkarray[2])-1; i++)
    {
        digitalWrite(ROWS[i],checkarray[2][i]);
    }

    if(digitalRead(COLS[0]) == HIGH && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
        Serial.println("9");
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == HIGH && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
        Serial.println("10");  
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == HIGH && digitalRead(COLS[3]) == LOW){
        Serial.println("11");
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == HIGH){
        Serial.println("12");
    }else{;}

    for (int i = 0; i < sizeof(checkarray[3])-1; i++)
    {
        digitalWrite(ROWS[i],checkarray[3][i]);
    }

    if(digitalRead(COLS[0]) == HIGH && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
        Serial.println("13");
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == HIGH && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == LOW){
        Serial.println("14");  
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == HIGH && digitalRead(COLS[3]) == LOW){
        Serial.println("15");
    }else if(digitalRead(COLS[0]) == LOW && digitalRead(COLS[1]) == LOW && digitalRead(COLS[2]) == LOW && digitalRead(COLS[3]) == HIGH){
        Serial.println("16");
    }else{;}


    delay(100);
}

