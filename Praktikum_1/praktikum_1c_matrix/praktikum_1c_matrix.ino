/* Zeilen: PORT-C |  Spalten: PORT-A
  -------------------------
  | S1  | S2  | S3  | S4  | - 0 --> D37
  | S5  | S6  | S7  | S8  | - 1 --> D36
  | S9  | S10 | S11 | S12 | - 2 --> D35
  | S13 | S14 | S15 | S16 | - 3 --> D34
  -------------------------
    |      |     |     |
    7      6     5     4
   D29    D28   D27   D26

If Pin is OUTPUT: 1= HIGH,   0=LOW
If Pin is INPUT:  1= ENABLE, 0=Disable  Pull-up Resistor
*/

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

const uint8_t ROWS[4] = {0,1,2,3};  // Die ersten 4 Bits vom PORT-C  /*Serial.print("Counter: ");

const uint8_t COLS[4] = {7,6,5,4};  // Die letzten 4 Bits vom PORT-A
uint8_t row_index =0;
uint8_t col_index =0;
String pressed_key = "";

void setup() {

  Serial.begin(9600);
  lcd.begin();
  //lcd.init();             // für ander LiquidCrystal_I2C lib
  //lcd.backlight();

  DDRA &= B00001111;        // Spalten: Setze die letzten 4 Bits vom PORT-A als INPUT(0)
  DDRC |= B00001111;        // Zeilen:  Setze die ersten 4 Bits vom PORT-C als OUTPUT(1)
  PORTC = (1 << ROWS[0]);   // Direkt die erste Zeile auf HIGH(1) setzen

  cli();                    // interrupts deaktivieren

  TCNT1 = 0;                // setze timer1(16bit) auf 0
  TCCR1B |= B00000001;      // kein(1)-prescaler notwendig

  TIMSK1 |= B00000010;      // aktiviert OCIE1A, also den compare match mode für OCR1A
  OCR1A = 16;               // alle 1000ns: 1000(ns)*16.000.000 / ( 1 * 10^(9) ) = 16


  sei();                    // interrupts aktivieren
}

void loop() {

  lcd.clear();
  lcd.print(pressed_key);
  Serial.println(pressed_key);
  delay(500);
}


// Timer Interrupt, wird alle 1000 ns aufgerufen
ISR(TIMER1_COMPA_vect){

  // Welche der 4 Spalten wurde gedrückt?
  if( PINA & (1 << COLS[col_index]) ){
    pressed_key = row_index*4 + col_index + 1;  // Tastenfeld[row_index][col_i];
  }

  col_index = (col_index+1) %4;                 // Spaltenindex für nächsten Interrupt auswählen
  if(col_index == 0){
    row_index = (row_index+1) %4;               // Näcshte Reihe auf HIGH(1) setzen
    PORTC = (1 << ROWS[row_index]);
  }

  TCNT1 = 0;                            // Setze timer1 wieder auf 0
}
