/* 
  Zeiteinheit t: s=0, ms=3, ns=9
  Bei Timer1 sollte OCRA < 65536 sein
  OCRxy = Zeit(s, ms, ns) * clockspeed / ( prescaler * 10^t )
  
  Jede Sekunde mit Zeiteinheit s ohne prescaler:
  OCRxy = 1(s)*16.000.000 / ( 1 * 10^0 ) = 16.000.000      <-- passt nicht in 65536

  Jede Sekunde mit Zeiteinheit ms und prescaler 256
  OCRxy = 1000(ms)*16.000.000 / ( 256 * 10^3 ) = 62.500    <-- passt rein

  Jede Millisekunde mit Zeiteinheit ms ohne prescaler:
  OCRxy = 1(ms)*16.000.000 / ( 1 * 10^(3) ) = 16.000       <-- passt 

  Jede Millisekunde mit Zeiteinheit ns: 1ms -> 1.000.000ns und prescaler 64:
  OCRxy = 1.000.000(ns)*16.000.000 / ( 64 * 10^(9) ) = 250 <-- passt 
*/
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
// Konstanten
LiquidCrystal_I2C lcd(0x27, 16, 2);
const uint8_t red_btn = 4;        // D2 -> PORTE 4
const uint8_t blue_btn = 5;       // D3 -> PORTE 5
const uint8_t red_led = 5;        // D8 -> PORTH 5
const uint8_t blue_led = 6;       // D9 -> PORTH 6
const uint8_t debounce = 10;      // Entprellzeit in ms


// Variablen
int pressTime = 0;
uint8_t button_reader = 0;
volatile int red_cnt = 0;
volatile int blue_cnt = 0;
bool inc_pressed = false;
bool dec_pressed = false;

void setup() {
  
  Serial.begin(9600);
  lcd.begin();
  print_counter();

  // LEDS: D8 und D9 als OUTPUT setzen
  DDRH |= B01100000;

  // Buttons als Input setzen, ohne pullup
  DDRE |= B00110000;      // 1=Input, 0=Output
  PORTE &= B11001111;     // pullup-resistor: 1=enable, 0=disable
  
  cli();                  // interrupts deaktivieren

  // Timer1 für Blaue LED, TIMSK und OCRxy werden im loop dynamisch gesetzt
  TCCR3A = 0;                // Reset entire TCCR1A to 0 
  TCCR3B = B00000100;        // Set CS32 to 1 so we get prescalar 256  

  // Timer3 für Rote LED, TIMSK und OCRxy werden im loop dynamisch gesetzt
  TCCR4A = 0;                 // Reset entire TCCR1A to 0 
  TCCR4B = B00000100;         // Set CS32 to 1 so we get prescalar 256  
  
  //   7     6     5     4     3     2     1     0
  // ISC71 ISC70 ISC61 ISC60 ISC51 ISC50 ISC41 ISC40
  // RISING edge generates interrupt on int4 and int5
  EICRB = B00001111;        // ((1 << ISC40) | (1 << ISC41) | (1 << ISC50) | (1 << ISC51));
  
  // Enable interrupts for int4(D2) and int5(D3)
  EIMSK = B00110000;//((1 << INT4) | (1 << INT5));
  
  TCNT1 = 0;                // setze timer1(16bit) auf 0
  TCCR1B |= B00000001;      // kein(1)-prescaler notwendig

  TIMSK1 |= B00000010;      // aktiviert OCIE1A, also den compare match mode für OCR1A
  OCR1A = 16000;            // jede ms: 1(ms)*16.000.000 / ( 1 * 10^(3) ) = 16.000 
  
  sei();                    // interrupts aktivieren
}

void loop() {
  print_counter();
  // pretend to calculate heavy stuff
  delay(300);
}

// Timer Interrupt, wird jede ms aufgerufen
ISR(TIMER1_COMPA_vect){
  
  /* RED LED section */
  if(inc_pressed){
    pressTime++;
    /* Long press? */
    if( (PINE & _BV(red_btn)) >> red_btn ){ // 1: gedrückt, 0:nada
      if(pressTime > 1000){
        red_cnt = 0;
        pressTime = 0;
      }
    }
    /* Short press? */
    else if( pressTime > debounce){
      red_cnt = (red_cnt+1) %4;
      pressTime = 0;
      inc_pressed = false;
    }
  }

  /* BLUE section */
  else if(dec_pressed){
    pressTime++;
    /* Long press? */
    if( (PINE & _BV(blue_btn)) >> blue_btn ){ // 1: gedrückt, 0:nada
      if(pressTime > 1000){
        blue_cnt = 0;
        pressTime = 0;
      }
    }
    /* Short press? */
    else if(pressTime > debounce){
      blue_cnt = (blue_cnt+1) %4;
      pressTime = 0;
      dec_pressed = false;
    }
  }
  
  TCNT1  = 0; // setze timer1 wieder auf 0
}

// Timer für BLUE LED
ISR(TIMER3_COMPA_vect){
  TCNT3  = 0;                     // reset timer3
  PORTH ^= (1 << blue_led);       // Invert blue led
}

// Timer für RED LED
ISR(TIMER4_COMPB_vect){      
  TCNT4  = 0;                     // reset timer4    
  PORTH ^= (1 << red_led);        // Invert red led
}

// Increment button
ISR(INT4_vect) {
    inc_pressed= true; 
}

// Decrement button
ISR(INT5_vect) {
    dec_pressed = true;  
}

void print_counter(){
  
  if(red_cnt == 0){
    TCNT4 = 0;
    TIMSK4 &= ~(1 << OCIE4B);       // disables compare match mode -> no interrupts
    PORTH &= ~(1 << red_led);       // Enable blue led
  }
  else if(red_cnt ==1){
    TIMSK4 |= (1 << OCIE4B);        // enable compare match mode
    OCR4B = 62500;                  // 1(s)*16.000.000/256 -> every Second
  }
  else if(red_cnt==2){
    TIMSK4 |= (1 << OCIE4B);        // enable compare match mode
    OCR4B = 31250;                  // 500(ms)*16.000.000/256*10^3 -> every 500ms
  }
  else if(red_cnt==3){  
    TIMSK4 |= (1 << OCIE4B);        // enable compare match mode
    OCR4B = 6250;                   // 100(ms)*16.000.000/256*10^3 -> every 100ms
  }
  
  if(blue_cnt == 0){
    TCNT3 = 0;
    TIMSK3 = 0;
    PORTH &= ~(1 << blue_led);       // Enable blue led
  }
  else if(blue_cnt == 1){
    TIMSK3 |= (1 << OCIE3A);        // enable compare match mode
    OCR3A = 62500;                   // 1(s)*16.000.000/256 -> every Second
  }
  else if(blue_cnt == 2){
    TIMSK3 |= (1 << OCIE3A);        // enable compare match mode
    OCR3A = 31250;                   // 500(ms)*16.000.000/256*10^3 -> every 500ms
  }
  else if(blue_cnt == 3){
    TIMSK3 |= (1 << OCIE3A);        // enable compare match mode
    OCR3A = 6250;                   // 100(ms)*16.000.000/256*10^3 -> every 100ms
  }
  
  lcd.clear();
  lcd.setCursor(0,1);
  lcd.print("Blue:" +String(blue_cnt));
  lcd.print(" Red:" +String(red_cnt));
}
