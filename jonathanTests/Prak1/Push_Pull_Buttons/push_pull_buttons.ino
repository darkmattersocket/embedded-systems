/* wenn read=1 --> 0v 
   wenn read=0 --> 5v 
*/
//PULL gedrückt: 1(0v),  nicht gedrückt: 0 (5v)
uint8_t pullButtonPin = 2; // PORT-E: 2 -> 4

// PUSH gedrückt: 0(5v), nicht gedrückt: 1 (0v)
uint8_t pushButtonPin = 3; // PORT-E: 3 -> 5

//LEDs
uint8_t blueLedPin = 8;  // PORT-H: 8 -> 5
uint8_t greenLedPin = 9; // PORT-H: 9 -> 6

// States
uint8_t pullButtonRead, pushButtonRead;
uint8_t pullVolt = 5,pushVolt = 0;
  
void setup() {
  Serial.begin(9600);
  // DDRH = B01100000; // setze LED pins als OUTPUT
  // PORTE = B00110000; // setze Button pins als INPUT
  pinMode(blueLedPin, OUTPUT);
  pinMode(greenLedPin, OUTPUT);
  pinMode(pullButtonPin, INPUT);
  pinMode(pushButtonPin, INPUT);
}

void loop() {
  pullButtonRead = digitalRead(pullButtonPin);
  pushButtonRead = digitalRead(pushButtonPin);
  // pullButtonRead = (PINE & _BV(pullButtonPin)) >> pullButtonPin;
  // pushButtonRead = (PINE & _BV(pushButtonPin)) >> pushButtonPin;
  
  pullVolt = pushVolt = 5;
  if(pullButtonRead == HIGH){
    pullVolt =0;
    digitalWrite(blueLedPin, HIGH);
    // PORTH = _BV(blueLedPin) | PORTH;
  }
  else{
    digitalWrite(blueLedPin, LOW);
    // PORTH = ~_BV(blueLedPin) & PORTH;
  }
  
  if(pushButtonRead){
    pushVolt =0;
    digitalWrite(greenLedPin, HIGH);
    // PORTH = _BV(greenLedPin) | PORTH;
  }
  else{
    digitalWrite(greenLedPin, LOW);
    // PORTH = ~_BV(greenLedPin) & PORTH;
  }

  my_print_function("pushButton: ", pushButtonRead, pushVolt);
  my_print_function("pullButton: ", pullButtonRead, pullVolt);
  
  Serial.println();
  delay(100);
}

void my_print_function(String name, uint8_t state, uint8_t volt){
  Serial.print(name);
  Serial.print(state);
  
  Serial.print(" (");
  Serial.print(volt);
  Serial.print("v)   ");
}
