/* 
  Zeiteinheit t: s=0, ms=3, ns=9
  Bei Timer1 sollte OCRA < 65536 sein
  OCRxy = Zeit(s, ms, ns) * clockspeed / ( prescaler * 10^t )
  
  Jede Sekunde mit Zeiteinheit s ohne prescaler:
  OCRxy = 1(s)*16.000.000 / ( 1 * 10^0 ) = 16.000.000      <-- passt nicht in 65536

  Jede Sekunde mit Zeiteinheit ms und prescaler 256
  OCRxy = 1000(ms)*16.000.000 / ( 256 * 10^3 ) = 62.500    <-- passt rein

  Jede Millisekunde mit Zeiteinheit ms ohne prescaler:
  OCRxy = 1(ms)*16.000.000 / ( 1 * 10^(3) ) = 16.000       <-- passt 

  Jede Millisekunde mit Zeiteinheit ns: 1ms -> 1.000.000ns und prescaler 64:
  OCRxy = 1.000.000(ns)*16.000.000 / ( 64 * 10^(9) ) = 250 <-- passt 
*/
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
// Konstanten
LiquidCrystal_I2C lcd(0x27, 16, 2);
const uint8_t inc_btn = 4;        // D2 -> PORTE 4
const uint8_t dec_btn = 5;        // D3 -> PORTE 5
const uint8_t debounce = 10;      // Entprellzeit in ms


// Variablen
int pressTime = 0;
uint8_t button_reader = 0;
volatile int cnt = 0;
bool inc_pressed = false;
bool dec_pressed = false;

void setup() {
  
  Serial.begin(9600);
  lcd.begin();
  print_counter();

  DDRE |= B00110000;      // 1=Input, 0=Output
  PORTE &= B11001111;     // pullup-resistor: 1=enable, 0=disable
  
  cli();                  // interrupts deaktivieren
  
  //   7     6     5     4     3     2     1     0
  // ISC71 ISC70 ISC61 ISC60 ISC51 ISC50 ISC41 ISC40
  // RISING edge generates interrupt on int4 and int5
  EICRB = B00001111;        // ((1 << ISC40) | (1 << ISC41) | (1 << ISC50) | (1 << ISC51));
  
  // Enable interrupts for int4(D2) and int5(D3)
  EIMSK = B00110000;//((1 << INT4) | (1 << INT5));
  
  TCNT1 = 0;                // setze timer1(16bit) auf 0
  TCCR1B |= B00000001;      // kein(1)-prescaler notwendig

  TIMSK1 |= B00000010;      // aktiviert OCIE1A, also den compare match mode für OCR1A
  OCR1A = 16000;            // jede ms: 1(ms)*16.000.000 / ( 1 * 10^(3) ) = 16.000 
  
  sei();                    // interrupts aktivieren
}

void loop() {
  print_counter();
  // pretend to calculate heavy stuff
  delay(1000);
}

// Timer Interrupt, wird jede ms aufgerufen
ISR(TIMER1_COMPA_vect){
  
  /* Increment section */
  if(inc_pressed){
    pressTime++;
    /* Long press? */
    if( (PINE & _BV(inc_btn)) >> inc_btn ){ // 1: gedrückt, 0:nada
      if(pressTime > 1000){
        cnt += 10;
        pressTime = 0;
      }
    }
    /* Short press? */
    else if(pressTime > debounce){
      cnt++;
      pressTime = 0;
      inc_pressed = false;
    }
  }

  /* Decrement section */
  else if(dec_pressed){
    pressTime++;
    /* Long press? */
    if( (PINE & _BV(dec_btn)) >> dec_btn ){ // 1: gedrückt, 0:nada
      if(pressTime > 1000){
        cnt -= 10;
        pressTime = 0;
      }
    }
    /* Short press? */
    else if(pressTime > debounce){
      cnt--;
      pressTime = 0;
      dec_pressed = false;
    }
  }
  
  TCNT1  = 0; // setze timer1 wieder auf 0
}
ISR(INT4_vect) {
    inc_pressed= true; 
}
ISR(INT5_vect) {
    dec_pressed = true;  
}

void print_counter(){
  lcd.clear();
  lcd.print("Count: ");
  lcd.print(cnt);
  /*Serial.print("Counter: ");
  Serial.println(cnt);*/
}
