#define inputDT 4   // D2
#define inputCLK 5  // D3

uint8_t current_clk;
uint8_t previous_clk;

volatile int counter =0;
volatile uint8_t clockwise =0;
String angular_direction[2] = {"Counter-Clockwise", "Clockwise"};

void setup() {
  Serial.begin (9600);
  
  DDRE &= B00110000;        // Setze CLK und DT als INPUT(0)
  
  cli();                    // interrupts deaktivieren

  TCNT1 = 0;                // setze timer1(16bit) auf 0
  TCCR1B |= B00000001;      // kein(1)-prescaler notwendig

  TIMSK1 |= B00000010;      // aktiviert OCIE1A, also den compare match mode für OCR1A
  OCR1A = 16000;            // jede ms: 1(ms)*16.000.000 / ( 1 * 10^(3) ) = 16.000

  sei();                    // interrupts aktivieren
 
  previous_clk = (PINE & (1 << inputCLK)) >> inputCLK; //digitalRead(inputCLK); 

}

void loop() {
  Serial.print(angular_direction[clockwise]);
  Serial.print(" -- ");
  Serial.println(counter);  
  delay(500);
}

// Timer Interrupt, wird jede ms aufgerufen
ISR(TIMER1_COMPA_vect){

  current_clk = (PINE & (1 << inputCLK)) >> inputCLK;
  
  // Wenn der letzte und der aktuelle Pin-Status unterschiedlich sind, wurde gedreht
  if (current_clk != previous_clk && current_clk == HIGH){ 
    
      // Wenn inputDT nicht gleich ist wie inputCLK, dann wurde im Uhrzeigersinn gedreht
      if ( ((PINE & (1 << inputDT)) >> inputDT) != current_clk) { 
        counter ++;
        clockwise =1;
       
      } 
      else {
        // Ansonsten gegen den Uhrzeigersinn
        counter --;
        clockwise =0;
      }
   }

   previous_clk = current_clk;            // Merke den letzten Pin-Status
   TCNT1 = 0;                             // Setze timer1 wieder auf 0
}
