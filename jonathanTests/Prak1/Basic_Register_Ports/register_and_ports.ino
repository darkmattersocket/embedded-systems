const uint8_t led_pin = 7; // 7 am PORT-C und 30 aufm Arduino
const uint8_t btn_pin = 6; // 6 am PORT-C und 31 aufm Arduino

uint8_t btn_read;

void setup() {
  DDRC = pow(2, led_pin); // setze led als OUTPUT(1): 2^7 = 128 = B10000000
  PORTC = pow(2, btn_pin); // setze btn als INPUT: 2^6 = 64 = B01000000 --> aktiviert pullup-resistor
}

/*
 * _BV(bit) steht für: (1 << (bit))
 * und is einfacher zu lesen
 */
void loop() {
  btn_read = (PINC & _BV(btn_pin)) >> btn_pin;
  
  if(btn_read == LOW){
    PORTC = _BV(led_pin) | PORTC;
  }
  else{
    PORTC = ~_BV(led_pin) & PORTC;
  }
}

/*
const uint8_t led_pin = 30;
const uint8_t btn_pin = 31;
void setup(){
  pinMode(btn_pin, INPUT_PULLUP);
  pinMode(led_pin, OUTPUT);
}

void loop() {
  btn_read = digitalRead(btn_pin);
  
  if(btn_read == LOW){
    digitalWrite(led_pin, HIGH);
  }
  else{
    digitalWrite(led_pin, LOW);
  }
}
*/
