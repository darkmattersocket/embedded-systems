/* Zeilen: PORT-C |  Spalten: PORT-A
  -------------------------
  | S1  | S2  | S3  | S4  | - 0 --> D37
  | S5  | S6  | S7  | S8  | - 1 --> D36
  | S9  | S10 | S11 | S12 | - 2 --> D35
  | S13 | S14 | S15 | S16 | - 3 --> D34
  -------------------------
    |      |     |     |
    7      6     5     4
   D29    D28   D27   D26

If Pin is OUTPUT: 1= HIGH,   0=LOW  
If Pin is INPUT:  1= ENABLE, 0=Disable  Pull-up Resistor
*/ 

int ROWS[4] = {0,1,2,3};  // Die ersten 4 Bits vom PORT-C
int COLS[4] = {7,6,5,4};  // Die letzten 4 Bits vom PORT-A

String matrix[4][4] = 
{
  {"S1",  "S2",   "S3",   "S4"},
  {"S5",  "S6",   "S7",   "S8"},
  {"S9",  "S10",  "S11",  "S12"},
  {"S13", "S14",  "S15",  "S16"}
};

void setup() {

  DDRA &= B00001111;   // COLS INPUT(0)
  DDRC |= B00001111;   // ROWS OUTPUT(1)
  Serial.begin(9600);
}
 
void loop() {
  
    for(int j=0;j<4;j++){
      // Jede Reihe einmal auf HIGH setzen...
      PORTC = (1 << ROWS[j]);

      // und dann schauen, welche Spalte auch HIGH ist
      for(int i=0;i<4;i++){
        if( (PINA & (1 << COLS[i])) >> COLS[i] == HIGH){
          Serial.print("You pressed: ");
          Serial.println(matrix[j][i]);
          Serial.println();
          break;
        }  
      }
    }
  delay(100);
}
