/*         PORT-C
  -------------------------
  | S1  | S2  | S3  | S4  | - 3 --> D34
  | S5  | S6  | S7  | S8  | - 2 --> D35
  | S9  | S10 | S11 | S12 | - 1 --> D36
  | S13 | S14 | S15 | S16 | - 0 --> D37
  -------------------------
    |      |     |     |
    4      5     6     7
   D33    D32   D31   D30
*/ 

int ROWS[4] = {34,35,36,37};  // Reihe der 1. Zeile
int COLS[4] = {33,32,31,30};  // Spalten der 1. Zeile

String matrix[4][4] = 
{
  {"S1",  "S2",   "S3",   "S4"},
  {"S5",  "S6",   "S7",   "S8"},
  {"S9",  "S10",  "S11",  "S12"},
  {"S13", "S14",  "S15",  "S16"}
};

void setup() {
  
  for(int i=0;i<4;i++){
    // ROWS
    pinMode(ROWS[i], OUTPUT);
    digitalWrite(ROWS[i], LOW);

    // COLS
    pinMode(COLS[i], INPUT);
    digitalWrite(COLS[i], LOW);
  }
  
  Serial.begin(9600); //For printing out the output
}
 
void loop() {
    for(int j=0;j<4;j++){
      digitalWrite(ROWS[0], LOW);
      digitalWrite(ROWS[1], LOW);
      digitalWrite(ROWS[2], LOW);
      digitalWrite(ROWS[3], LOW);
      digitalWrite(ROWS[j], HIGH);

      for(int i=0;i<4;i++){
        if(digitalRead(COLS[i]) == HIGH){
          Serial.print("You pressed: ");
          Serial.println(matrix[j][i]);
          Serial.println();
        }  
      }
    }
  delay(100);
}
