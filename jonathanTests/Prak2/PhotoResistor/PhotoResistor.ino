int lightPen =A4;
int lightVal;

void setup() {
  // put your setup code here, to run once:
  pinMode(lightPen, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  lightVal = analogRead(lightPen);
  Serial.println(lightVal);

  delay(250);
}
