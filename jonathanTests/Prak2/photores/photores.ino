int lightPen =A4;
int lightVal;

int dv = 250;
int red = 2;
int green = 3;

void setup() {
  // put your setup code here, to run once:
  pinMode(lightPen, INPUT);
  pinMode(red, OUTPUT);
  pinMode(green, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  lightVal = analogRead(lightPen);
  Serial.println(lightVal);

  delay(250);

  if(lightVal > 650){
    digitalWrite(green, HIGH);
    digitalWrite(red, LOW);
  }
  else{
    digitalWrite(green, LOW);
    digitalWrite(red, HIGH);
  }
}
