#define EVL A6

void setup() {
  pinMode(EVL, INPUT);
  Serial.begin(9600);
}

void loop() {
  int reader = analogRead(EVL);

  Serial.print("Raw: ");
  Serial.println(reader);

  delay(250);
}
