int KTY = 2;
int raw;
float R1 = 10000;
float logR2, R2, T;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

void setup() {
Serial.begin(9600);
}

void loop() {

  raw = analogRead(KTY);

  double Temp;
  Temp = log(((10240000/raw) - 10000));
  Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
  Temp = Temp - 273.15;           // Convert Kelvin to Celcius
  
  Serial.print("raw: "); 
  Serial.print(raw);
  Serial.print("  Temperature: "); 
  Serial.print(Temp);
  Serial.println(" °"); 

  delay(500);
}
