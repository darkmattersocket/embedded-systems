/* S E N S O R K E T T E: 
Sensoren bilden eine physikalische Größe auf eine elektrische Größe ab. 
Wenn wir Größen wie Widerstandsänderung, Spannung, Strom, Frequenz, Kapazität und Induktion messen können,
dann können wir wiederum alle physikalischen Größen daraus ableiten.
Eingangs Spannung (0 - 5v) -> Ad wandler -> Eingangs Spannungsberechung -> Elektrischegröße rückrechnen -> Pysikalische größe
*/

#define temparatursensor    A0  //LM 35 DZ
#define temparatursenso2    A2  //KTY 81-110
#define fotowiederstand     A4  
#define ambientelichtsensor A6  //EVL ALS-PDIC144
#define hallsensor          A8 


void setup()
{
    Serial.begin(9600);
}

// Photoresistor 
void print_pr() 
{
    int fotowiederstand_value = analogRead(fotowiederstand);
    Serial.print("Fotowiederstand: ");
    Serial.print(fotowiederstand_value);
}

// EVL ALS-PDIC144 Ambient Light 
// Eingangs Spannung (0 - 5v) -> Ad wandler -> U read * (  5 / 1024) -> i * 1000000 / 0.5
void print_ldr() 
{
    int ambientelichtsensor_value = analogRead(ambientelichtsensor);
    float u = ambientelichtsensor_value * (5.0 / 1024);     // U read * (  vmax / maxvel)
    float r = ((5.0 / u) - 1) * 10000;                      // R = ((Uref / Us)) - 1) * R1
    float i = u / r;                                        // I = U / R
    float ambientLux = i * 1000000 / 0.5;
    Serial.print("Ambientelichtsensor: ");
    Serial.print(ambientLux);
    Serial.print(" lux");
}

// LM 35 DZ
// Eingangs Spannung (0 - 5v) -> Ad wandler -> *5000/1024 -> /10
void print_temperature1()
{
    int temparatur_value = analogRead(temparatursensor);        // input wer
    float voltage = temparatur_value * (5000 / 1024.0);         // input zu volt convertierung ( volt / 10bit )
    float celsius = voltage / 10;                               // volt convert to celsius v / 10
    Serial.print("Temparatur: ");
    Serial.print(celsius);
    Serial.print("*C (LM35)");
}

// KTY 81-110
// Eingangs Spannung (0 - 5v) -> Ad wandler -> log(((10240000 / temparatur) - 10000)) -> 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp))- 273.15
void print_temperature2()
{
    int temparatur = analogRead(temparatursenso2);
    double Temp = log(((10240000 / temparatur) - 10000));
    Temp = 1 / (0.001129148 + (0.000234125 * Temp) + (0.0000000876741 * Temp * Temp * Temp));
    Temp = Temp - 273.15;
    Serial.print("Temparatur2: ");
    Serial.print(Temp);
    Serial.print("*C (KTY81)");
}


// SS495A hall-sensor
void print_hall()
{
    uint16_t gaussIn = analogRead(hallsensor);
    int range = 30;
    int gaussMeter = map(gaussIn, 0, 1024, 0, range); // Map input to range from 0 to 30 for console out
    int gauss = map(gaussIn, 102, 922, -640, 640); // Calgulate gauss by mapping input to range from -640 to 640
    Serial.print("- [");
    for (int i = 0; i < gaussMeter; i++)
    {
        if (i == (range / 2) - 1)
        {
            Serial.print("|");
        }
        else
        {
            Serial.print("-");
        }
    }
    Serial.print("O");
    for (int i = gaussMeter + 1; i < 30; i++)
    {
        if (i == (range / 2) - 1)
        {
            Serial.print("|");
        }
        else
        {
            Serial.print("-");
        }
    }
    Serial.print("] + ");
    Serial.print(gauss);
    Serial.print(" Gauss");
}

void loop()
{
    print_pr();
    Serial.print("         ");
    print_ldr();
    Serial.print("         ");
    print_temperature1();
    Serial.print("         ");
    print_temperature2();
    Serial.print("         ");
    print_hall();
    Serial.println("");

    delay(100);
}
