#include <Wire.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 16, 2);

#define inputDT 1  // D52
#define inputCLK 0 // D53

uint8_t current_clk;
uint8_t previous_clk;

int counter = 0;
String x = "";
void setup()
{
  Serial.begin(9600);
  lcd.begin();
  //lcd.init();         // für ander LiquidCrystal_I2C lib
  //lcd.backlight();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Haloooo");

  DDRB &= B00000011; // Setze CLK und DT als INPUT(0)

  cli(); // interrupts deaktivieren

  TCNT1 = 0;           // setze timer1(16bit) auf 0
  TCCR1B |= B00000001; // kein(1)-prescaler notwendig

  TIMSK1 |= B00000010; // aktiviert OCIE1A, also den compare match mode für OCR1A
  OCR1A = 16000;       // jede ms: 1(ms)*16.000.000 / ( 1 * 10^(3) ) = 16.000

  sei(); // interrupts aktivieren

  previous_clk = (PINE & (1 << inputCLK)) >> inputCLK; //digitalRead(inputCLK);
}

void loop()
{
  Serial.println(counter);
  lcd.clear();
  lcd.print(counter);
  delay(100);
}

// Timer Interrupt, wird jede ms aufgerufen
ISR(TIMER1_COMPA_vect)
{

  current_clk = (PINB & (1 << inputCLK)) >> inputCLK;

  // Wenn der letzte und der aktuelle Pin-Status unterschiedlich sind, wurde gedreht
  if (current_clk != previous_clk && current_clk == HIGH)
  {
    // Wenn inputDT nicht gleich ist wie inputCLK, dann wurde gegen den Uhrzeigersinn gedreht
    if (((PINB & (1 << inputDT)) >> inputDT) != current_clk)
    {
      counter--;
    }
    else
    {
      // Ansonsten im Uhrzeigersinn
      counter++;
    }
    x = counter;
  }

  previous_clk = current_clk; // Merke den letzten Pin-Status
  TCNT1 = 0;                  // Setze timer1 wieder auf 0
}
