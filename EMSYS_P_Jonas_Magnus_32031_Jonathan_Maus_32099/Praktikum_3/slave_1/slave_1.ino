/*  Slave 1 */
#define LM35 A0
#define KELVIN 273.15
int raw;
int celcius;
float kelvin;
static byte index;
static boolean receiving;
const uint8_t ledPin = 7; // 7 am PORT-L --> D42
const byte commandLength = 6;
const char startChar = '*';
const char stopChar = '#';
char masterInput[6];
char readChar;


void setup() {
  Serial.begin(9600);
  DDRL |= B10000000;    // setzt LED als OUTPUT
  PORTL |= B10000000;   // lass LED ersmal aus
}

void loop() {
  receiveCommand();
  delay(500);
}

void receiveCommand() {
    receiving = false;
    index = 0;

    // Lese char für char den Befehl ein
    while (Serial.available() > 0 ) {
        readChar = Serial.read();
        if (receiving) {
            if (readChar != stopChar) {
                masterInput[index] = readChar;
                index++;
                if (index>= commandLength) {
                    index = commandLength - 1;
                }
            }
            else {// stopChar(#) ist angekommen, Befehl fertig gelesen
              masterInput[index] = '\0';
              receiving = false;
              index = 0;

              handle_command();
            }
        }

        else if (readChar == startChar) {
            receiving = true;// startChar(*) ist angekommen, Befehl wird jetzt gelesen
        }
    }
}

void handle_command(){
  Serial.print("Received: ");
  Serial.print(masterInput);
  
  if(masterInput[0] == 'L' && masterInput[1] == '0' && masterInput[2] == '\0'){
    PORTL &= B01111111; // LED aus
    Serial.println(" --> LED aus");
  }
  else if(masterInput[0] == 'L' && masterInput[1] == '1' && masterInput[2] == '\0'){
    PORTL |= B10000000; // LED ein
    Serial.println(" --> LED an");
  }
  else if(masterInput[0] == 'L' && masterInput[1] == '\0'){
    const uint8_t state = ( PORTL >> ledPin);
    Serial.print(" --> LED State: ");
    Serial.println(state);
  }
  else if(masterInput[0] == 't' && masterInput[1] == '\0'){
    raw = analogRead(LM35);
    celcius = int(raw * (500/1024.0));
    Serial.print(" --> Sending Celcius: ");
    Serial.println(celcius);
  }
  else{
    Serial.println(" --> Unknown Command");
  }
}
